class ProgressBar {
	id: string // 모듈에 부착할 노드의 id
	color: string

	private progressBar = document.createElement('div')
	private container = document.createElement('div')
	private icon = document.createElement('span')
	private img = document.createElement('img')

	constructor(id: string, color: string) {
		this.id = id
		this.color = color

		this.event()
	}

	// 추후 옵션으로 지정 해둘 get 들
	get setIcon() {
		this.icon.setAttribute('class', 'icon')
		this.img.setAttribute('class', 'img')
		this.img.src = './src/img/ic_car_control.png'

		this.icon.appendChild(this.img)

		return this.icon
	}

	get setProgressBar() {
		this.progressBar.setAttribute('class', 'progressbar')
		this.progressBar.style.backgroundColor = this.color

		return this.progressBar
	}

	// 컨테이너에 아이콘과 프로그레스 바 부착
	get setContainer() {
		this.container.setAttribute('class', 'container')
		this.container.appendChild(this.setProgressBar)

		return this.container
	}

	// 테스트 목적으로 만든 이벤트
	event() {
		window.addEventListener('scroll', e => {
			this.setProgressBar.style.width = this.scrolled + '%'
			this.setIcon.style.transform = `translateX(${this.scrolled}%`
		})
	}

	// 해당 아이디 값을 가진 노드에 container 부착
	setup() {
		const element = document.querySelector(`#${this.id}`) as HTMLDivElement
		element.appendChild(this.setContainer)
		element.appendChild(this.setIcon)
	}

	// 스크롤 시 기능
	// 스크롤 시 페이지 상단의 위치값을 반환
	get winScroll() {
		return document.body.scrollTop || document.documentElement.scrollTop
	}

	// 스크롤 시키지 않을 때 전체 높이에서 클라이언트가 스크롤 시켰을 때의 높이 차
	get preHeight() {
		return document.documentElement.scrollHeight - document.documentElement.clientHeight
	}

	// 상단의 위치 값에 높이 차를 나눈 퍼센트 값 반환
	get scrolled() {
		return (this.winScroll / this.preHeight) * 100
	}
}

const movv = new ProgressBar('main', '#38b0de')
movv.setup()
