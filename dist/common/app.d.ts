export declare type Option = {
    icon?: HTMLSpanElement;
    img?: HTMLImageElement;
    color?: string;
    types?: string;
};
