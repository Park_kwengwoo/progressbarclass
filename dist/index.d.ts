declare class ProgressBar {
    id: string;
    color: string;
    private progressBar;
    private container;
    private icon;
    private img;
    constructor(id: string, color: string);
    get setIcon(): HTMLSpanElement;
    get setProgressBar(): HTMLDivElement;
    get setContainer(): HTMLDivElement;
    event(): void;
    setup(): void;
    get winScroll(): number;
    get preHeight(): number;
    get scrolled(): number;
}
declare const movv: ProgressBar;
